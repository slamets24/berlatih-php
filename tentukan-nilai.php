<?php
function tentukan_nilai($number){
	$output = "";
	if ($number >= 85) {
		$output = "<h1>Sangat Baik ". $number."</h1><br>";
	}elseif ($number >= 70) {
		$output = "<h1>Baik ". $number."</h1><br>";
	}elseif ($number >= 60){
		$output = "<h1>Cukup ". $number."</h1><br>";
	}else{
		$output = "<h1>Kurang ". $number."</h1><br>";
	}
	return $output;
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>